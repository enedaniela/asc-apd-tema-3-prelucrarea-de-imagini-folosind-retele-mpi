#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>



int main(int argc, char * argv[]) {
	int rank;
	int nProcesses;
	FILE *ft, *fc, *fi;
	char * line = NULL;
    size_t len = 0;
    ssize_t read;
   	char buff[255];
   	const char s[3] = ": ";
    char *token;
    int num;
    int rows = 4000, col = 4000;
    int pixel;
    char filterName[20];
	char nameIn[20];
	char nameOut[20];
	char map[10][3][20];
	int totalImages;
	int N;

	int i, j, k = 0, rowsRecv, ord, t, rowsRecvP, factor = 1,v,p;
	int filter[3][3];
	int smoothFilter[][3] = {{1, 1, 1},{1, 1, 1},{1, 1, 1}};
    int blurFilter[][3] = {{1, 2, 1},{2, 4, 2},{1, 2, 1}};
    int sharpenFilter[][3] = {{0, -2, 0},{-2, 11, -2},{0, -2, 0}};
    int meanRemovalFilter[][3] = {{-1, -1, -1},{-1, 9, -1},{-1, -1, -1}};
	int ceva, filterType = 1, filterTypeR;

	MPI_Init(&argc, &argv);
	MPI_Status status;
	MPI_Request request;
	

	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &nProcesses);

	N = nProcesses;
	int probe[N], response[N];

	int** matToSend = malloc((rows + 2) * sizeof(int*)); 
  	int** matricePrelucrata = malloc((rows + 2) * sizeof(int*));
  	int** finalMat = calloc(rows + 2, sizeof(int*));
  	int** matriceRecv = malloc((rows + 2) * sizeof(int*));
	for(i = 0; i < rows + 2; i++){
		matToSend[i] = malloc((col + 2) * sizeof(int));
		matricePrelucrata[i] = malloc((col + 2) * sizeof(int));
		finalMat[i] = calloc(col + 2, sizeof(int));
		matriceRecv[i] = malloc((col + 2) * sizeof(int));
	}

	int parent[N], noNeigh = 0, neigboursInit[N], neigbours[N], statistica[N], statisticaR[N];
	for (i = 0; i < N; i++)
	{
		parent[i] = -1;
		statistica[i] = 0;
		statisticaR[i] = 0;
	}

   	ft = fopen(argv[1], "r");
   	
   	if (ft == NULL)
        exit(EXIT_FAILURE);

    while ((read = getline(&line, &len, ft)) != -1) {
   
	   token = strtok(line, s);
	    num = atoi(token);	   
	   
	   if(rank == num){

		   while( token != NULL ) 
		   {
		    
		    int elem = atoi(token);
		    if(elem != rank){
			    neigboursInit[noNeigh] = elem;
				noNeigh++;
			}
		    token = strtok(NULL, s);
		   }
	}    
    }

	if (rank == 0)
	{
		for (i = 0; i < noNeigh; i++)
		{
			
			MPI_Send(probe, N, MPI_INT, neigboursInit[i], 0, MPI_COMM_WORLD);
		}
		for (i = 0; i < noNeigh; i++)
		{
			MPI_Recv(&response, N, MPI_INT, MPI_ANY_SOURCE, 1, MPI_COMM_WORLD, &status);
			for(int k = 0; k < N; k++)
				if (parent[k] == -1) 
					parent[k] = response[k];
		}


		for (i = 0; i < noNeigh; i++)
		{
			MPI_Send(parent, N, MPI_INT, neigboursInit[i], 2, MPI_COMM_WORLD);
		}
	}
	else
	{
		MPI_Recv(probe, N, MPI_INT, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, &status);
		parent[rank] = status.MPI_SOURCE;
		for (i = 0; i < noNeigh; i++)
		{
			if (neigboursInit[i] != parent[rank])
			{
				MPI_Send(probe, N, MPI_INT, neigboursInit[i], 0, MPI_COMM_WORLD);
			}
		}

		for (i = 0; i < noNeigh-1; i++)
		{
			MPI_Recv(response, N, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
			if (status.MPI_TAG == 0) 
				continue;
			for(int k = 0; k < N; k++)
				if (parent[k] == -1) parent[k] = response[k];
		}

		MPI_Send(parent, N, MPI_INT, parent[rank], 1, MPI_COMM_WORLD);

		// RECV topologie
		MPI_Recv(parent, N, MPI_INT, parent[rank], 2, MPI_COMM_WORLD, &status);

		for (i = 0; i < N; i++)
		{
			if (parent[i] == rank)
				MPI_Send(parent, N, MPI_INT, i, 2, MPI_COMM_WORLD);
		}
	}
	MPI_Barrier(MPI_COMM_WORLD);
	
	noNeigh = 0;
	if(rank != 0){
		neigbours[0] = parent[rank];
		noNeigh++;
	}
	for(i = 0; i < N; i++){

		if(parent[i] == rank){
			neigbours[noNeigh] = i;
			noNeigh++;
		}
		
	}

	MPI_Barrier(MPI_COMM_WORLD);
	
	if(rank == 0){

		fi = fopen(argv[2], "r");

		if (fi == NULL)
	        exit(EXIT_FAILURE);

	    fscanf(fi, "%d\n", &totalImages);

	    for(i = 0; i < totalImages; i++){
		    fscanf(fi, "%s %s %s\n", map[i][0], map[i][1], map[i][2]);
		}
		fclose(fi);
	}

	if(rank == 0){
		for(i = 0; i < noNeigh; i++)
			MPI_Send(&totalImages, 1, MPI_INT, neigbours[i], 0, MPI_COMM_WORLD);
	}
	else{
		MPI_Recv(&totalImages, 1, MPI_INT, parent[rank], 0, MPI_COMM_WORLD, &status);
		if(noNeigh != 1){
			for(i = 1; i < noNeigh; i++)
				MPI_Send(&totalImages, 1, MPI_INT, neigbours[i], 0, MPI_COMM_WORLD);

		}
	}

	MPI_Barrier(MPI_COMM_WORLD);
	
	for(p = 0; p < totalImages; p++){
	MPI_Barrier(MPI_COMM_WORLD);

	if(rank == 0){
		if(strcmp(map[p][0],"smooth") == 0){
			filterType = 1;
		}
		if(strcmp(map[p][0],"blur") == 0){
			filterType = 2;
		}
		if(strcmp(map[p][0],"sharpen") == 0)
			filterType = 3;
		if(strcmp(map[p][0],"mean_removal") == 0)
			filterType = 4;
		fc = fopen(map[p][1], "r");
		if (fc == NULL)
	        exit(EXIT_FAILURE);
		char buffer[100];
		fgets(buffer, 100, fc);
		fgets(buffer, 100, fc);//sar peste primele 2 linii
		fscanf(fc, "%d %d\n", &col, &rows);

	  	col += 2;//adaug coloanele bordura

		fgets(buffer, 100, fc);//sar peste 255

		for(i = 0; i < rows + 2; i++)
		{
		    for(j = 0; j < col; j++) 
		    {
			    matToSend[i][j] = 0;
			    
		    }
		}

		for(i = 1; i < rows - 1 + 2; i++)
		{
		    for(j = 1; j < col - 1; j++) 
		    {
			    fscanf(fc, "%d\n", &matToSend[i][j]);  
			    
		    }
		}
		fclose(fc);
		
		FILE *final;
		final = fopen(map[p][2],"w");
		//rows trebuie sa fie numarul real
		int share = rows / noNeigh;
		int remains = rows % noNeigh;
		//trimit numarul de randuri pe care trebuie sa le primeasca vecinii, numarul real, fara borduri
		for(i = 0; i < noNeigh - 1; i++){
			//trimit filtrul
			MPI_Send(&filterType, 1, MPI_INT, neigbours[i], 0, MPI_COMM_WORLD);
			//trimit si numarul de coloane
			MPI_Send(&col, 1, MPI_INT, neigbours[i], 0, MPI_COMM_WORLD);
			//trimit numarul de linii
			if(share > 0){
				MPI_Send(&share, 1, MPI_INT, neigbours[i], 0, MPI_COMM_WORLD);
			}
			else if(i <= remains){
				ceva = share + 1;
				MPI_Send(&ceva, 1, MPI_INT, neigbours[i], 0, MPI_COMM_WORLD);
			}
			else{
				ceva = 0;
				MPI_Send(&ceva, 1, MPI_INT, neigbours[i], 0, MPI_COMM_WORLD);	
			}

		}

		if(remains >= 0 && share > 0){
			//trimit filtrul
			MPI_Send(&filterType, 1, MPI_INT, neigbours[i], 0, MPI_COMM_WORLD);
			//trimit si numarul de  coloane
			MPI_Send(&col, 1, MPI_INT, neigbours[i], 0, MPI_COMM_WORLD);
			ceva = remains + share;
			MPI_Send(&ceva, 1, MPI_INT, neigbours[noNeigh - 1], 0, MPI_COMM_WORLD);//daca am un rest mai mare ca 0
		}

		if(share == 0 && remains < noNeigh){
			MPI_Send(&filterType, 1, MPI_INT, neigbours[noNeigh - 1], 0, MPI_COMM_WORLD);
			//trimit si numarul coloane
			MPI_Send(&col, 1, MPI_INT, neigbours[noNeigh - 1], 0, MPI_COMM_WORLD);
			ceva = 0;
			MPI_Send(&ceva, 1, MPI_INT, neigbours[noNeigh - 1], 0, MPI_COMM_WORLD);
		}
		
		k = 0;

		//trimit blocul
		for (i = 0; i < noNeigh - 1; i++)
		{
			
			if(share > 0){
				for(j = k; j < share + 2 + k; j++){
					MPI_Send(matToSend[j], col, MPI_INT, neigbours[i], 0, MPI_COMM_WORLD);
				}
				k += share;
			}
			else if(i <= remains){
				for(j = k; j < 1 + 2 + k; j++){
					MPI_Send(matToSend[j], col, MPI_INT, neigbours[i], 0, MPI_COMM_WORLD);
				}
				k++;
			}
		}
		if(remains >= 0 && share > 0){
			for(j = k; j < share + remains + k + 2; j++){
				MPI_Send(matToSend[j], col, MPI_INT, neigbours[noNeigh - 1], 0, MPI_COMM_WORLD);
			}
		}

		//primire linii prelucrate
		//concatenare
		ord = 0;
		//astept liniile prelucrate si le concatenez in matricePrelucrata
		for(v = 0; v < noNeigh; v++){
			MPI_Recv(&rowsRecv, 1, MPI_INT, neigbours[v], 0, MPI_COMM_WORLD, &status);
			for(i = 0; i < rowsRecv; i++){
				MPI_Recv(matricePrelucrata[i + ord], col - 2, MPI_INT, neigbours[v], 0, MPI_COMM_WORLD, &status);
			}
			ord += rowsRecv;
		}
		//scriere in fisier
		fprintf(final, "P2\n");
		fprintf(final, "# CREATOR: GIMP PNM Filter Version 1.1\n");
		fprintf(final, "%d %d\n", col - 2, ord);
		fprintf(final, "255\n");
		for(i = 0; i < ord; i++){
			for(j = 0; j < col - 2; j++)
				fprintf(final, "%d\n", matricePrelucrata[i][j]);
		}

		for( i = 0; i < noNeigh; i++){
			MPI_Recv(statisticaR, N, MPI_INT, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, &status);
			for(j = 0; j < N; j++){
					statistica[j] += statisticaR[j];
			}
		}
	
		
		fclose(final);
	}
	else{

		//primesc filtrul
		MPI_Recv(&filterTypeR, 1, MPI_INT, parent[rank], 0, MPI_COMM_WORLD, &status);

		//primesc numarul de coloane
		MPI_Recv(&col, 1, MPI_INT, parent[rank], 0, MPI_COMM_WORLD, &status);

		//primesc numarul de randuri de la parinte
		MPI_Recv(&rowsRecv, 1, MPI_INT, parent[rank], 0, MPI_COMM_WORLD, &status);
	
		if(rowsRecv != 0){
		//primesc randurile
		for (i = 0; i < rowsRecv + 2; i++)
		{
			MPI_Recv(matriceRecv[i], col, MPI_INT, parent[rank], 0, MPI_COMM_WORLD, &status);
		}
	}
		
		//daca nu sunt frunza, impart si trimit mai departe
		if(noNeigh != 1){
			int share = rowsRecv / (noNeigh - 1);//nu il calculez si pe parinte
			int remains = rowsRecv % (noNeigh - 1);

			//trimit numarul de randuri pe care trebuie sa le primeasca vecinii, numarul real, fara borduri
			for(i = 1; i < noNeigh - 1; i++){
				//trimit filtrul
				MPI_Send(&filterTypeR, 1, MPI_INT, neigbours[i], 0, MPI_COMM_WORLD);
				//trimit si numarul de coloane
				MPI_Send(&col, 1, MPI_INT, neigbours[i], 0, MPI_COMM_WORLD);
				//trimit numarul de linii
				if(share > 0){
					MPI_Send(&share, 1, MPI_INT, neigbours[i], 0, MPI_COMM_WORLD);
				}
				else if(i <= remains){
					ceva = share + 1;
					MPI_Send(&ceva, 1, MPI_INT, neigbours[i], 0, MPI_COMM_WORLD);
				}
				else{
					ceva = 0;
					MPI_Send(&ceva, 1, MPI_INT, neigbours[i], 0, MPI_COMM_WORLD);	
				}			
			}

			if(remains >= 0 && share > 0){
				//trimit filtrul
				MPI_Send(&filterTypeR, 1, MPI_INT, neigbours[noNeigh - 1], 0, MPI_COMM_WORLD);
				//trimit si numarul de coloane
				MPI_Send(&col, 1, MPI_INT, neigbours[noNeigh - 1], 0, MPI_COMM_WORLD);
				ceva = remains + share;
				MPI_Send(&ceva, 1, MPI_INT, neigbours[noNeigh - 1], 0, MPI_COMM_WORLD);//daca am un rest mai mare ca 0

			}
			if(share == 0 && remains < noNeigh - 1){
				
				MPI_Send(&filterType, 1, MPI_INT, neigbours[noNeigh - 1], 0, MPI_COMM_WORLD);
				//trimit si numarul de coloane
				MPI_Send(&col, 1, MPI_INT, neigbours[noNeigh - 1], 0, MPI_COMM_WORLD);
				ceva = 0;
				MPI_Send(&ceva, 1, MPI_INT, neigbours[noNeigh - 1], 0, MPI_COMM_WORLD);
		}
		
		//trimit blocul
		k = 0;
		for (v = 1; v < noNeigh - 1; v++)
		{
			
			if(share > 0){
				for(j = k; j < share + 2 + k; j++){
					MPI_Send(matriceRecv[j], col, MPI_INT, neigbours[v], 0, MPI_COMM_WORLD);
				}
				k += share;
			}
			else if(v <= remains){
				for(j = k; j < 1 + 2 + k; j++){
					MPI_Send(matriceRecv[j], col, MPI_INT, neigbours[v], 0, MPI_COMM_WORLD);
				}
				k++;
			}
		}
		if(remains >= 0 && share > 0){
			for(j = k; j < share + remains + k + 2; j++){
				MPI_Send(matriceRecv[j], col, MPI_INT, neigbours[noNeigh - 1], 0, MPI_COMM_WORLD);
			}
		}

		ord = 0;
		//astept liniile prelucrate si le concatenez in matricePrelucrata
		for(v = 1; v < noNeigh; v++){
			if(parent[rank] != neigbours[v]){
				MPI_Recv(&rowsRecvP, 1, MPI_INT, neigbours[v], 0, MPI_COMM_WORLD, &status);

				for(i = 0; i < rowsRecvP; i++){
					MPI_Recv(matricePrelucrata[i + ord], col - 2, MPI_INT, neigbours[v], 0, MPI_COMM_WORLD, &status);
				}
				ord += rowsRecvP;
			}
		}
		//astept statistica de la toti vecinii
		for( i = 1; i < noNeigh; i++){
			MPI_Recv(statisticaR, N, MPI_INT, neigbours[i], 0, MPI_COMM_WORLD, &status);

				for(j = 0; j < N; j++)
					if(statisticaR[j] != 0)
						statistica[j] = statisticaR[j];
		}
		
		//trimit mai sus
		//trimit numarul de linii pe care ma pregatesc sa le trimit parintelui, fara borduri
			MPI_Send(&ord, 1, MPI_INT, parent[rank], 0, MPI_COMM_WORLD);
			//trimit noua matrice la parinte, doar cu informatia utila
			for(i = 0; i < ord; i++){
				MPI_Send(matricePrelucrata[i], col - 2, MPI_INT, parent[rank], 0, MPI_COMM_WORLD);
			}
		
		//trimit statistica
			MPI_Send(statistica, N, MPI_INT, parent[rank], 0, MPI_COMM_WORLD);
		}
		//daca sunt frunza, procesez
		else{
			//initializez matricea in care calculez
			for(i = 0; i < rowsRecv; i++){
				for(j = 0; j < col - 2; j++)
					finalMat[i][j] = 0;
			}

			//aleg filtrul
			if(filterTypeR == 1){
				for(i = 0; i < 3; i++)
					for(j = 0; j < 3; j++)
						filter[i][j] = smoothFilter[i][j];
			}
			if(filterTypeR == 2){
				for(i = 0; i < 3; i++)
					for(j = 0; j < 3; j++)
						filter[i][j] = blurFilter[i][j];
			}
			if(filterTypeR == 3){
				for(i = 0; i < 3; i++)
					for(j = 0; j < 3; j++)
						filter[i][j] = sharpenFilter[i][j];
			}
			if(filterTypeR == 4){
				for(i = 0; i < 3; i++)
					for(j = 0; j < 3; j++)
						filter[i][j] = meanRemovalFilter[i][j];
			}
			
			//aplic filtrul pe noua matrice
			factor = 0;
			for(i = 0; i < 3; i++)
				for(j = 0; j < 3; j++)
					factor += filter[i][j];

			for(k = 1; k < rowsRecv + 2 - 1; k++)
				for(i = 1; i < col - 1; i++){
					finalMat[k - 1][i - 1] =  	(matriceRecv[k - 1][i - 1] * filter[0][0] +
												matriceRecv[k - 1][i] * filter[0][1] +
												matriceRecv[k - 1][i + 1] * filter[0][2] +
												matriceRecv[k][i - 1] * filter[1][0] +
												matriceRecv[k][i] * filter[1][1] +
												matriceRecv[k][i + 1] * filter[1][2] +
												matriceRecv[k + 1][i - 1] * filter[2][0] +
												matriceRecv[k + 1][i] * filter[2][1] +
												matriceRecv[k + 1][i + 1] * filter[2][2]) / factor;

					if(finalMat[k - 1][i - 1] < 0)
						finalMat[k - 1][i - 1] = 0;
					if(finalMat[k - 1][i - 1] > 255)
						finalMat[k - 1][i - 1] = 255;
				}


			//trimit numarul de linii pe care ma pregatesc sa le trimit parintelui, fara borduri			
			MPI_Send(&rowsRecv, 1, MPI_INT, parent[rank], 0, MPI_COMM_WORLD);
		
			//trimit noua matrice la parinte, doar cu informatia utila
			
			for(i = 0; i < rowsRecv; i++){
				MPI_Send(finalMat[i], col - 2, MPI_INT, parent[rank], 0, MPI_COMM_WORLD);
			}
			statistica[rank] = rowsRecv;
			//trimit statistica
			MPI_Send(statistica, N, MPI_INT, parent[rank], 0, MPI_COMM_WORLD);

		}
	}
}
	fclose(ft);

	MPI_Barrier(MPI_COMM_WORLD);

	if(rank == 0){
		FILE *fstat;
		fstat = fopen(argv[3], "w");
		for(i = 0; i < N; i++)
			fprintf(fstat,"%d: %d\n", i, statistica[i]);
		fclose(fstat);
	}

	MPI_Finalize();
	
	return 0;
}
